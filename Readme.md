# Somsmai home line bot
Integrate Line bot with IBM chat bot (Watson Assistant v2)

### Reference Documentation
For further reference, please consider the following sections:
* [Heroku Doc](https://devcenter.heroku.com/articles/getting-started-with-java#deploy-the-app)
* [line bot SDK java](https://github.com/line/line-bot-sdk-java/tree/master/sample-spring-boot-echo)
* [ngrok for expose local port to test webhook](https://dashboard.ngrok.com/get-started/setup)
* [IBM Watson Assistant v2 SDK](https://github.com/watson-developer-cloud/java-sdk/tree/master/assistant)
* [IBM Watson Assistant v2 API Doc](https://cloud.ibm.com/apidocs/assistant/assistant-v2?code=java#create-a-session)
