package somsmai.bot.home.constance;

import lombok.Getter;

import java.util.Arrays;
import java.util.List;

@Getter
public enum Keyword {

    HELP("help", "-h"),
    CRYPTO_LIST("crypto", "coin list"),
    CRYPTO_MARKET_PRICE("price");

    Keyword(String... keys) {
        this.keys = Arrays.asList(keys);
    }

    private List<String> keys;

}
