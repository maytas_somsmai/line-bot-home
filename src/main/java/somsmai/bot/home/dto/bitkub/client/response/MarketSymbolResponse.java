package somsmai.bot.home.dto.bitkub.client.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import somsmai.bot.home.dto.bitkub.client.CoinSymbol;

import java.util.List;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class MarketSymbolResponse extends BasicBitkubResponse {

    private List<CoinSymbol> result;

}
