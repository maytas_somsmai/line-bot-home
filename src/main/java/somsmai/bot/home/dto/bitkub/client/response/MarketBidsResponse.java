package somsmai.bot.home.dto.bitkub.client.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import somsmai.bot.home.dto.bitkub.client.BidDetail;

import java.util.List;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class MarketBidsResponse extends BasicBitkubResponse {

    private List<BidDetail> result;

}
