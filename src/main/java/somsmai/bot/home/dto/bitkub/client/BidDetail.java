package somsmai.bot.home.dto.bitkub.client;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BidDetail {

    private int order;

    private int timestamp;

    private String volume;

    private String rate;

    private String amount;

}
