package somsmai.bot.home.dto.bitkub.client;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CoinSymbol {

    private Integer id;
    private String symbol;
    private String info;

}
