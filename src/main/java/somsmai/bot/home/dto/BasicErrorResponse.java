package somsmai.bot.home.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import somsmai.bot.home.constance.ErrorCode;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BasicErrorResponse {

    private ErrorCode errorCode;

    private String message;

}
