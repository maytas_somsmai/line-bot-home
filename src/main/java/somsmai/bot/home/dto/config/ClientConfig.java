package somsmai.bot.home.dto.config;

import lombok.Data;

import java.util.Map;

@Data
public class ClientConfig {

    private int timeOut;

    private String baseUrl;

    private Map<String, String> apiEndpoints;

}
