package somsmai.bot.home.dto.config;

import lombok.Data;

@Data
public class WatsonClientConfig extends ClientConfig{

    private String apiKey;

    private String assistantId;

}
