package somsmai.bot.home.config;

import io.netty.channel.ChannelOption;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;
import somsmai.bot.home.constance.ErrorCode;
import somsmai.bot.home.exception.ServiceException;

import javax.net.ssl.SSLException;

@Slf4j
@Component
@AllArgsConstructor
public class BitkubClientConfig {

    private final ApplicationConfig applicationConfig;

    @Bean(name = "bitkubClient")
    public WebClient bitkubClient() throws SSLException {
        return WebClient.builder()
                .clientConnector(httpConnector())
                .defaultHeader(
                        HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE
                )
                .filter(errorHandlingFilter())
                .baseUrl(applicationConfig.getBitkubClient().getBaseUrl())
                .build();
    }

    private ReactorClientHttpConnector httpConnector() throws SSLException {
        SslContext sslContext = SslContextBuilder
                .forClient()
                .trustManager(InsecureTrustManagerFactory.INSTANCE)
                .build();
        return new ReactorClientHttpConnector(
                HttpClient.create()
                        .secure(sslContextSpec -> sslContextSpec.sslContext(sslContext))
                        .tcpConfiguration(
                                tcpClient -> tcpClient.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, applicationConfig.getBitkubClient().getTimeOut() * 1000)
                                        .doOnConnected(connection -> connection
                                                .addHandlerLast(new ReadTimeoutHandler(applicationConfig.getBitkubClient().getTimeOut()))
                                                .addHandlerLast(new WriteTimeoutHandler(applicationConfig.getBitkubClient().getTimeOut()))
                                        )
                        )
        );
    }

    public ExchangeFilterFunction errorHandlingFilter() {
        return ExchangeFilterFunction.ofResponseProcessor(clientResponse -> {
            if (clientResponse.statusCode().isError()) {
                ServiceException ex = ServiceException.builder()
                        .httpStatus(clientResponse.statusCode())
                        .errorCode(ErrorCode.ERROR_FROM_BITKUB_CLIENT)
                        .build();
                try {
                    clientResponse.bodyToMono(String.class)
                            .flatMap(
                                    errorBody -> {
                                        ex.setMessage(errorBody);
                                        return Mono.error(ex);
                                    }
                            );
                } catch (Exception e) {
                    log.error("Cannot extract response body", e);
                }
                return Mono.error(ex);
            } else {
                return Mono.just(clientResponse);
            }
        });
    }

}
