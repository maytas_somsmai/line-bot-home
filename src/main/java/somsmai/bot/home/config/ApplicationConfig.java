package somsmai.bot.home.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import somsmai.bot.home.dto.config.ClientConfig;
import somsmai.bot.home.dto.config.WatsonClientConfig;

@Data
@Component
@ConfigurationProperties(prefix = "application")
public class ApplicationConfig {

    private ClientConfig bitkubClient;

    private WatsonClientConfig watsonClient;


}
