package somsmai.bot.home.service;

import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import somsmai.bot.home.client.BitkubClientService;
import somsmai.bot.home.dto.bitkub.client.BidDetail;
import somsmai.bot.home.dto.bitkub.client.CoinSymbol;

import java.util.stream.Collectors;

@Service
public class BitkubService {

    private final BitkubClientService bitkubClientService;

    public BitkubService(BitkubClientService bitkubClientService) {
        this.bitkubClientService = bitkubClientService;
    }

    public Mono<String> getAllMarketSymbols() {
        return bitkubClientService.getAllMarketSymbols()
                .flatMap(response -> Mono.just(
                        response.getResult().stream()
                                .map(CoinSymbol::getSymbol).collect(Collectors.joining("\n"))
                        )
                );
    }

    public Mono<String> getMarketBit(String coinSymbol) {
        return bitkubClientService.getMarketBit(coinSymbol)
                .flatMap(response -> Mono.just(
                        response.getResult().stream()
                                .map(BidDetail::getRate).collect(Collectors.joining("\n"))
                        )
                );
    }
}
