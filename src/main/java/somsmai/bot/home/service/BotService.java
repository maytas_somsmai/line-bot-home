package somsmai.bot.home.service;

import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.StickerMessageContent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.StickerMessage;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import somsmai.bot.home.client.WatsonClientService;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@LineMessageHandler
public class BotService {

    private final BitkubService bitkubService;
    private final WatsonClientService watsonClientService;

    public BotService(BitkubService bitkubService, WatsonClientService watsonClientService) {
        this.bitkubService = bitkubService;
        this.watsonClientService = watsonClientService;
    }

    @EventMapping
    public List<TextMessage> handleTextMessageEvent(MessageEvent<TextMessageContent> event) {
        log.info("Text event: " + event);
        final String originalMessageText = event.getMessage().getText();
        return watsonClientService.getMessageResponse(originalMessageText)
                .getOutput().getGeneric().stream()
                .map(g -> new TextMessage(g.text())).collect(Collectors.toList());
    }

    @EventMapping
    public Message handleStickerEvent(MessageEvent<StickerMessageContent> event) {
        log.info("Sticker event: " + event);
        return new StickerMessage(event.getMessage().getPackageId(), event.getMessage().getStickerId());
    }

    @EventMapping
    public void handleDefaultMessageEvent(Event event) {
        log.info("Default event: " + event);
    }

}
