package somsmai.bot.home.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import somsmai.bot.home.constance.ErrorCode;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ServiceException extends RuntimeException{

    private HttpStatus httpStatus;

    private ErrorCode errorCode;

    private String message;

}
