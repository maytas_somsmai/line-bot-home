package somsmai.bot.home.client;

import com.ibm.cloud.sdk.core.security.Authenticator;
import com.ibm.cloud.sdk.core.security.IamAuthenticator;
import com.ibm.watson.assistant.v2.Assistant;
import com.ibm.watson.assistant.v2.model.*;
import org.springframework.stereotype.Service;
import somsmai.bot.home.config.ApplicationConfig;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class WatsonClientService {

    private Assistant assistant;

    private SessionResponse response;

    private final ApplicationConfig applicationConfig;

    public WatsonClientService(ApplicationConfig applicationConfig) {
        this.applicationConfig = applicationConfig;
    }

    public MessageResponse getMessageResponse(String originalMessageText) {
        initConnection();

        MessageInput input = new MessageInput.Builder()
                .text(originalMessageText)
                .build();
        MessageOptions messageOptions = new MessageOptions.Builder()
                .assistantId(applicationConfig.getWatsonClient().getAssistantId())
                .sessionId(response.getSessionId())
                .input(input)
                .build();
        return assistant.message(messageOptions).execute().getResult();
    }

    private void initConnection() {
        Authenticator authenticator = new IamAuthenticator(applicationConfig.getWatsonClient().getApiKey());

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd"); //"2019-02-28"
        Date date = new Date();
        String dateStr = dateFormat.format(date);
        assistant = new Assistant(dateStr, authenticator);
        assistant.setServiceUrl(applicationConfig.getWatsonClient().getBaseUrl());
        CreateSessionOptions options = new CreateSessionOptions
                .Builder(applicationConfig.getWatsonClient().getAssistantId()).build();

        response = assistant.createSession(options).execute().getResult();
    }

}
