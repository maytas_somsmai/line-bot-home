package somsmai.bot.home.client;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import somsmai.bot.home.config.ApplicationConfig;
import somsmai.bot.home.constance.BitkubAPI;
import somsmai.bot.home.dto.bitkub.client.response.MarketBidsResponse;
import somsmai.bot.home.dto.bitkub.client.response.MarketSymbolResponse;

@Service
public class BitkubClientService {

    private final WebClient webClient;
    private final ApplicationConfig applicationConfig;

    public BitkubClientService(
            @Qualifier("bitkubClient") WebClient webClient,
            ApplicationConfig applicationConfig) {
        this.webClient = webClient;
        this.applicationConfig = applicationConfig;
    }

    public Mono<MarketSymbolResponse> getAllMarketSymbols() {
        return webClient.get()
                .uri(
                        applicationConfig.getBitkubClient().getApiEndpoints()
                                .get(BitkubAPI.MARKET_SYMBOLS.name())
                )
                .retrieve()
                .bodyToMono(MarketSymbolResponse.class);
    }

    public Mono<MarketBidsResponse> getMarketBit(String coinSymbol) {
        return webClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(applicationConfig.getBitkubClient().getApiEndpoints()
                                .get(BitkubAPI.MARKET_BIDS.name()))
                        .queryParam("sym", coinSymbol)
                        .queryParam("lmt", 5)
                        .build()
                )
                .retrieve()
                .bodyToMono(MarketBidsResponse.class);
    }

}
